<?php
/*
SQL Buddy - for Web based MySQL administration
https://sqlbuddy.org/

ajaxquery.php
- used for a variety of ajax functionality - runs a background query

MIT license

Copyright (c) 2020-2022 Chris Hutchinson <https://chrishutchinson.info>
Copyright (c) 2008 Calvin Lough, <http://calv.in>
*/

include "functions.php";

loginCheck();

if (isset($db))
	$conn->selectDB($db);

if (isset($_POST['query'])) {
	$queryList = splitQueryText($_POST['query']);
	
	foreach ($queryList as $query) {
		$sql = $conn->query($query);
	}
}

//return the first field from the first row
if (!isset($_POST['silent']) && $conn->isResultSet($sql)) {
	$row = $conn->fetchArray($sql);
	echo nl2br(htmlentities($row[0], ENT_QUOTES, 'UTF-8'));
}

?>
