<?php
/*
SQL Buddy - for Web based MySQL administration
https://sqlbuddy.org/

ajaxsavecolumnedit.php
- saves the details of a table column

MIT license

Copyright (c) 2020-2022 Chris Hutchinson <https://chrishutchinson.info>
Copyright (c) 2008 Calvin Lough, <http://calv.in>
*/

include "functions.php";

loginCheck();

if (isset($db))
	$conn->selectDB($db);

if (isset($_POST['runQuery'])) {
	$query = $_POST['runQuery'];
	
	$conn->query($query) or ($dbError = $conn->error());
	
	echo "{\n";
	echo "    \"formupdate\": \"" . $_GET['form'] . "\",\n";
	echo "    \"errormess\": \"";
	if (isset($dbError))
		echo $dbError;
	echo "\"\n";
	echo '}';
	
}

?>
