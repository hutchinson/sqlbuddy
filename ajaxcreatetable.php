<?php
/*
SQL Buddy - for Web based MySQL administration
https://sqlbuddy.org/

ajaxcreatetable.php
- called from dboverview.php to create a new table

MIT license

Copyright (c) 2020-2022 Chris Hutchinson <https://chrishutchinson.info>
Copyright (c) 2008 Calvin Lough, <http://calv.in>
*/

include "functions.php";

loginCheck();

if (isset($db))
	$conn->selectDB($db);

if (isset($_POST['query'])) {
	
	$queryList = splitQueryText($_POST['query']);
	
	foreach ($queryList as $query) {
		$sql = $conn->query($query) or ($dbError = $conn->error());
	}
	
	if (isset($dbError)) {
		echo $dbError;
	}
	
}

?>
