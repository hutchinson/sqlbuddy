SQL Buddy
===========

SQL Buddy is a phpMyAdmin alternative with a focus on usability.

![Screenshot 1](https://sqlbuddy.org/img/screenshot1.png)
![Screenshot 2](https://sqlbuddy.org/img/screenshot2.png)
![Screenshot 3](https://sqlbuddy.org/img/screenshot3.png)

## Features

* Create and drop databases and tables
* View, edit, insert and delete table rows
* Execute custom SQL and view the output
* Import and export databases and tables to SQL and CSV formats
* Add, edit and delete MySQL users
* Ships with 47 translations (including Esperanto!)

## Download
**[Download v1.4.0 as zip file](https://https://codeberg.org/hutchinson/sqlbuddy/sqlbuddy.zip)**  
**[Download v1.4.0 as tar.gz file](https://codeberg.org/hutchinson/sqlbuddy/archive/v.1.4.0.tar.gz)**

## Installation

* Download the file above and unzip it
* Upload the sqlbuddy folder to your web server
* Point your browser to the sqlbuddy folder
* Log in using your existing MySQL credentials

## Requirements

* PHP 7+
* MySQL 5+

## Help

If you need help, a forum is available at https://sqlbuddy.org

## License

Copyright &copy; 2020-2022 Chris Hutchinson <https://chrishutchinson.info>  
Copyright &copy; 2013 Calvin Lough <http://calv.in>

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
