<?php
/*
SQL Buddy - for Web based MySQL administration
https://sqlbuddy.org/

index.php
- output page structure, content loaded through ajax

MIT license

Copyright (c) 2020-2022 Chris Hutchinson <https://chrishutchinson.info>
Copyright (c) 2008 Calvin Lough, <http://calv.in>
*/

include "functions.php";

loginCheck(false);

outputPage();

?>
